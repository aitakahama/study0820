(() => {
    'use strict';

    const random = () => Math.floor(Math.random() * 101);

    const 生徒 = require('./生徒');
    const 学級 = require('./学級');

    const 一年二組 = new 学級(1, 2, [
        new 生徒('平野 雄一郎', random()),
        new 生徒('松崎 正道', random()),
        new 生徒('黒木 大和', random()),
        new 生徒('本郷 晃弘', random()),
        new 生徒('沖野 和真', random()),
        new 生徒('江村 優作', random()),
        new 生徒('谷津 清一', random()),
        new 生徒('黒島 政志', random()),
        new 生徒('佐合 大輝', random()),
    ]);

    console.log(`平均評価点は ${一年二組.平均評価点()} 点です。`);

    const 最高成績者 = 一年二組.最高成績者();
    console.log(`最高成績は${最高成績者.名前}の ${最高成績者.評価点} 点です。`);
})();

