const 生徒集合 = require('./生徒集合');

const privates = Symbol('privates');

class 学級 {
    /**
     * @param {number} 年
     * @param {number} 組
     * @param {生徒[]} students
     */
    constructor(年, 組, students) {
        this[privates] = {
            年,
            組,
            students: new 生徒集合(students),
        };
    }

    /**
     * @returns {number}
     */
    平均評価点() {
        return this[privates].students.平均評価点();
    }

    /**
     * @returns {生徒}
     */
    最高成績者() {
        return this[privates].students.最高成績者();
    }
}

module.exports = 学級;