(() => {
    'use strict';

    const ImmutableUser = require('./immutable-user');

    const sendLetter = (user) => {
        console.log(`Dear ${user.name}。 `);
        console.log('手紙を一通送りました。');
    };

    const sendEmail = (user) => {
        console.log(`Dear ${user.name}様。 (E-Mail)`);
        console.log('E-Mail を一通送りました。');
    };

    const user = new ImmutableUser('沖野 和真');
    sendLetter(user);
    sendEmail(user);
    console.log(`${user.name}の処理が終わりました。`);
})();